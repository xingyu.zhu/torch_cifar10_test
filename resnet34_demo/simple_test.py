import os, time
import datetime
import torch
import GPUtil, pynvml


pynvml.nvmlInit()
handle = pynvml.nvmlDeviceGetHandleByIndex(0)
print(pynvml.nvmlDeviceGetName(handle))

device = 'cpu'
if torch.cuda.is_available():
    device = 'cuda'
    print('cuda initiated')

info = pynvml.nvmlDeviceGetMemoryInfo(handle)

for i in range(10):
    print(datetime.datetime.now())
    mem_info = pynvml.nvmlDeviceGetMemoryInfo(handle)
    load_info = pynvml.nvmlDeviceGetUtilizationRates(handle)
    print(mem_info.used / mem_info.total)
    print(load_info.gpu, load_info.memory)
    print(datetime.datetime.now())
    time.sleep(2)

print("Done")