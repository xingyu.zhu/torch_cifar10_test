import os, sys
sys.path.append(os.getcwd())

import torch
from config import Config # pylint: disable=no-name-in-module
from tqdm import tqdm
from torch.utils.data import DataLoader
import torch.backends.cudnn as cudnn
import pynvml

import argparse

def test_model(network, dataloader, criterion, device, progress_bar=False):

    enumerator = enumerate(dataloader, 0)
    if progress_bar:
        enumerator = tqdm(enumerator, total=len(dataloader), miniters=int(len(dataloader)/100))
    network.zero_grad()

    loss = 0.0
    corrected, total = 0, 0

    for _, data in enumerator:

        inputs, labels = data
        if device != 'cpu':
            inputs, labels = inputs.to(device), labels.to(device)

        outputs = network(inputs)
        loss += criterion(outputs, labels).item()
        _, predicted = torch.Tensor.max(outputs.data, 1)
        corrected += (predicted == labels).sum().item()
        total += labels.size(0)
    
    return {'loss': loss / len(dataloader), 'acc': corrected / total}

if __name__ == "__main__":
    
    conf = Config(data_load=True)

    parser = argparse.ArgumentParser()
    parser.add_argument('-m', type=str, default='final.pth',
                        help='the .pth state dict model for testing')
    # parser.add_argument('-acc', action='store_true',
    #                     help='Test the accuracy or not')
    args = parser.parse_args()

    device = 'cpu'
    if torch.cuda.is_available():
        device = 'cuda'
        print('cuda initiated') 

    net = conf.net()
    net = net.to(device)
    if device != 'cpu':
        print("Running on gpu")
        pynvml.nvmlInit()
        handle = pynvml.nvmlDeviceGetHandleByIndex(0)
        print(pynvml.nvmlDeviceGetName(handle))
        net = torch.nn.DataParallel(net)
        cudnn.benchmark = True
    
    model = args.m
    if '/' not in model:
        model_full_path = os.path.join(conf.model_path, model)
        net.load_state_dict(torch.load(model_full_path))
    else:
        net.load_state_dict(torch.load(model))
    print('Network state loaded.')

    testloader = DataLoader(conf.testset, batch_size=conf.testing_batchsize, shuffle=False, num_workers=4)
    classes = conf.classes
    print(test_model(net, testloader, conf.criterion, device))