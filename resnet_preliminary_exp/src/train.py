import os, sys
sys.path.append(os.getcwd())

import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import torch.backends.cudnn as cudnn

import torchvision
import torchvision.transforms as transforms
import matplotlib.pyplot as plt
import numpy as np
from torch.utils.tensorboard import SummaryWriter
from torch.utils.data import DataLoader
from tqdm import tqdm

from config import Config # pylint: disable=no-name-in-module

from os.path import join as pathjoin
import shutil, os, argparse, datetime
from IPython import embed
import pynvml

device = 'cpu'
handle = None
if torch.cuda.is_available():
    device = 'cuda'
    pynvml.nvmlInit()
    print('cuda initiated')

conf = Config(data_load=True)

parser = argparse.ArgumentParser()
parser.add_argument('-l', type=str,help='continue training from a specific model')
parser.add_argument('-lep', type=int, help='the epoch to start from')
parser.add_argument('-c', action='store_true',help='continue training from the latest model by the default scheme')
parser.add_argument('-o', action='store_true',help='Overwrite everything and start training')
parser.add_argument('-tfrefresh', action='store_true',help='Remove existing tensorboard records')

args = parser.parse_args()

assert (((args.l is not None) and args.c) or ((args.l is not None) and args.o) or (args.c and args.o)) is not True, 'You may only choose one among continue, overwrite, and load.'

net = conf.net()
net = net.to(device)
if device == 'cuda':
    net = torch.nn.DataParallel(net)
    cudnn.benchmark = True

print('Performing experiments at {}'.format(os.getcwd()))
print('Log will be saved at {}'.format(conf.experiment_path))
print('computation graph initialized')
start_epoch = -1

# Load specific model
# Tensorboard may be a mess after this operation
if args.l is not None:
    start_epoch = args.lep
    assert args.lep is not None, 'You must specify a start epoch in argument -lep (if training from scratch, type -lep=-1)'
    assert os.path.isfile(args.l), 'Invalid model'
    print('Loading {}, starting at epoch {}').format(args.l, args.lep)
    net.load_state_dict(torch.load(args.l))

elif os.path.isdir(conf.experiment_path):
    print('Previous Training Record Exists.')
    
    # Continue training by loading latest.pthl from the models directory
    if args.c:
        latest_model = pathjoin(conf.model_path, 'latest.pthl')
        assert os.path.isfile(latest_model), 'no valid latest.pthl'
        meta = torch.load(latest_model)
        assert isinstance(meta, dict)
        start_epoch = meta['epoch']
        print('Loading ./runs/models/latest.pthl, starting at epoch {}'.format(start_epoch))
        net.load_state_dict(meta['state_dict'])
    
    # Overwrite everything
    elif args.o:
        print('Overwriting everything.')
        shutil.rmtree(conf.experiment_path)
        for direc in [conf.experiment_path, conf.model_path, conf.log_path]:
            os.mkdir(direc)
    
    else:
        print('Please either choose overwrite(-o), load(-l & -leq), or continue(-c) in argument')
        exit()

if args.tfrefresh:
    if os.path.isdir(conf.log_path):
        shutil.rmtree(conf.log_path)
        print('all tensorboard record removed')
    else:
        print('no tensorboard log exists')

for dirc in [conf.experiment_path, conf.model_path, conf.log_path]:
    if not os.path.isdir(dirc):
        print(dirc)
        os.makedirs(dirc)

def im_convert(img):
    img = img / 2 + 0.5     # unnormalize
    return img

trainloader = DataLoader(conf.trainset, batch_size=conf.training_batchsize, shuffle=True, num_workers=conf.train_provider_count)
classes = conf.classes

writer = SummaryWriter(conf.log_path)

dataiter = iter(trainloader)
images, labels = dataiter.next()
img_grid = torchvision.utils.make_grid(images)
writer.add_image('image samples', im_convert(img_grid))
try:
    writer.add_graph(net, images)
    print('network structure plotted')
except:
    print('failed visualizing network structure in tensorboard, using graphviz instead.')
    from torchviz import make_dot
    images = images.to(device)
    tmp_outputs = net(images)
    make_dot(tmp_outputs).render("network_structure", format="jpg")
    os.system('rm -f network_structure')
    pass

if start_epoch == -1:
    torch.save(net.state_dict(), pathjoin(conf.model_path, 'init.pth'))

criterion = conf.criterion
optimizer = conf.optimizer_conf(net)
scheduler = conf.lr_scheduler_conf(optimizer)

if device != 'cpu':
    print('running on GPU')
    pynvml.nvmlInit()
    handle = pynvml.nvmlDeviceGetHandleByIndex(0)
    print(pynvml.nvmlDeviceGetName(handle))

print('Start Training: {}'.format(datetime.datetime.now()))
for epoch in range(conf.epoch_num):  # loop over the dataset multiple times

    if epoch <= start_epoch:
        scheduler.step()
        continue

    running_loss = 0.0
    start_time = datetime.datetime.now()

    if device == 'cpu':
        tqdm_it_train = tqdm(enumerate(trainloader, 0), total=len(trainloader), miniters=int(len(trainloader)/3))
        tqdm_it_train.set_description('Epoch {}'.format(epoch))
    else:
        tqdm_it_train = enumerate(trainloader, 0)
    
    for i, data in tqdm_it_train:

        inputs, labels = data
        inputs, labels = inputs.to(device), labels.to(device)
        optimizer.zero_grad()

        outputs = net(inputs)
        loss = criterion(outputs, labels)
        loss.backward()
        optimizer.step()

        # scheduler.step()

        running_loss += loss.item()
        update_freq = 50

        if i % update_freq == update_freq - 1:    # print every 2000 mini-batches
            avg_loss = running_loss / update_freq
            if device == 'cpu':
                tqdm_it_train.set_postfix_str('loss:{:.3f}'.format(avg_loss))
            writer.add_scalar('training_loss', avg_loss, epoch * len(trainloader) * conf.training_batchsize + i * conf.training_batchsize)
            writer.add_scalar('learning_rate', scheduler.get_last_lr()[0], epoch * len(trainloader) * conf.training_batchsize + i * conf.training_batchsize)
            running_loss = 0.0
        
        if device != 'cpu':
            gpu_update_freq = 100
            if i % gpu_update_freq == update_freq - 1:
                mem_info = pynvml.nvmlDeviceGetMemoryInfo(handle)
                load_info = pynvml.nvmlDeviceGetUtilizationRates(handle)
                writer.add_scalar('GPURAM_usage', mem_info.used / mem_info.total, epoch * len(trainloader) * conf.training_batchsize + i * conf.training_batchsize)
                writer.add_scalar('GPU_usage', load_info.gpu, epoch * len(trainloader) * conf.training_batchsize + i * conf.training_batchsize)
    
    if epoch % conf.snapshot_freq == conf.snapshot_freq - 1:
        torch.save(net.state_dict(), pathjoin(conf.model_path, 'epoch{}.pth'.format(epoch)))

    torch.save({'state_dict':net.state_dict(), 'epoch':epoch}, pathjoin(conf.model_path, 'latest.pthl'))
    end_time = datetime.datetime.now()
    print("Epoch {} Finished\t Training Loss {}\t Runtime: {}\t Logtime: {}".format(epoch, avg_loss, end_time - start_time, end_time))
    if avg_loss < conf.stop_by_criterion:
        print("Criterion less than designated value, abort training.")
        break
    scheduler.step()

print('Finished Training. The last snapshot saved to final.pth')
torch.save(net.state_dict(), pathjoin(conf.model_path, 'final.pth'))